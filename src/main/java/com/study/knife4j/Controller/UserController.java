package com.study.knife4j.Controller;

import com.study.knife4j.entities.UserAddRequest;
import com.study.knife4j.entities.UserEditRequest;
import com.study.knife4j.entities.UserQueryRequest;
import com.study.knife4j.entities.UserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Api(tags = "爱吃香蕉的孝坚")
@RestController
@RequestMapping("/user")
public class UserController {

    @ApiOperation("添加")
    @PostMapping("/add")
    public UserVO add(@RequestBody @Valid UserAddRequest userAddRequest) {
        // 将数据写到数据库
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userAddRequest, userVO);
        userVO.setId(1L);
        userVO.setCreateTime(LocalDateTime.now());
        userVO.setUpdateTime(LocalDateTime.now());
        return userVO;
    }

    @ApiOperation("修改")
    @PostMapping("/edit")
    public UserVO edit(@RequestBody @Valid UserEditRequest userEditRequest) {
        // 修改数据库的数据
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(userEditRequest, userVO);
        userVO.setUpdateTime(LocalDateTime.now());
        return userVO;
    }

    @ApiOperation("查找")
    @GetMapping("/find")
    public List<UserVO> find(UserQueryRequest userQueryRequest) {
        return new ArrayList<>();
    }

    @ApiOperation("删除")
    @PostMapping("/delete")
    public void delete(Long id) {
        // 将数据库数据删除
    }
}

