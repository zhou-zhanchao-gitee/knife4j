package com.study.knife4j.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class Knife4jConfig {

    @Bean(value = "defaultApi2")
    public Docket defaultApi2() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                        //.title("swagger-bootstrap-ui-demo RESTful APIs")
                        .description("3班账户管理系统接口文档")
                        .termsOfServiceUrl("http://www.xx.com/")
                        .contact(new Contact("test", "https://baidu.com", "xx@qq.com"))
                        .version("1.0")
                        .build())
                //分组名称
                .groupName("3班开发小组")
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage("com.study.knife4j.Controller"))
                .paths(PathSelectors.any())
                .build();
        return docket;

    }
}
